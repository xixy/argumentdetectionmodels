#coding=utf-8
import os

class hyperparameter(object):
	"""超参数"""
	def __init__(self, args):

		self.output_dir = args.dirpath 
		self.dir_model = self.output_dir + '/test/model.weights'
		self.log_path = self.output_dir + '/test/log.txt'
		self.dir_output = self.output_dir + '/test'
		self.evaluate_output_all_file = self.output_dir + '/result.all.txt'
		self.evaluate_output_wrong_file = self.output_dir + '/result.wrong.txt'
		self.evaluate_output_class_wrong_file = self.output_dir + '/result.class.wrong.txt'
		self.add_argument_role = args.add_argument_role
		print(self.output_dir)

		# 判断log文件是否存在，不存在就创建
		if not os.path.exists(self.output_dir):
			os.makedirs(self.output_dir)

		if not os.path.exists(self.dir_output):
			os.makedirs(self.dir_output)

		if not os.path.exists(self.log_path):
			log_file = open(self.log_path, 'w')
			log_file.close()

		self.epochs = 1500
		self.least_epochs = 25
		self.dropout = 0.5 # dropout概率
		self.batch_size = 100
		self.optimizer = 'adam'
		self.learning_rate = 0.01
		self.learning_rate_decay = 0.9
		self.num_of_epoch_no_imprv = 8 # early stop 如果几个epoch没有提高，那么就停止训练
		self.clip = -1 # 梯度控制
		self.hidden_size_lstm = 300
		self.use_crf = True
		self.use_pretrained_embeddings = True

		self.distance_embedding_dimension = 20
		self.distance_max = 600
		self.distance_min = -600

		self.char_dimension = 100

		self.entity_subtype_embedding_dimension = 5
		self.argument_role_embedding_dimension = 5
		self.event_type_embedding_dimension = 5

		self.max_length_sentence = 223

		# CNN的参数
		self.cnn_kernel_size = 5
		self.cnn_kernel_nums = 200

		self.use_joint_model = True
		self.joint_lambda_1 = 0.0
		self.joint_lambda_2 = 0.0
		self.joint_lambda_3 = 0.0
		self.joint_lambda_4 = 0.0