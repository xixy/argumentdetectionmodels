#coding=utf-8

def get_entities(entity_types):
	"""
	根据entity的标注结果来获取entity的列表
	"""
	entity_list = []
	start_idx, idx = None, 0
	while idx < len(entity_types):
		# 如果是B
		if entity_types[idx] < 49:
			# 如果是 BB 或者BIB
			if start_idx is not None:
				entity_list.append([start_idx, idx - 1])
				start_idx = idx
			else:
				start_idx = idx
		# 如果是O
		elif entity_types[idx] == 98:
			if start_idx is not None:
				entity_list.append([start_idx, idx - 1])
				start_idx = None
		idx += 1
	# 处理剩下的未处理的部分
	if start_idx is not None:
		entity_list.append([start_idx, idx - 1])
	return entity_list

def get_distance(length, anchor, offset = 0):
	"""
	根据anchor的位置来返回distance向量
	length:
		序列长度
	anchor: [start_idx, end_idx], 可能表示trigger和entity的位置
	offset: 为了完成正数的表示，需要偏移多少
	"""
	start_idx, end_idx = anchor[0], anchor[1]
	distance = [idx - start_idx if idx < start_idx else idx - end_idx for idx in range(length)]
	distance = distance[:start_idx] + [0 for _ in range(start_idx, end_idx)] + distance[end_idx:]
	distance = [x + offset for x in distance]
	return distance

if __name__ == '__main__':
	print(get_entities([1,1,1,1,88,88,89,98,1]))
	print(get_distance(10, [5,6], 5))