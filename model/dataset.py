#coding=utf-8
import random
from ArgumentDetectionModels.configure import *
def strQ2B(ustring):
	"""全角转半角"""
	rstring = ""
	for uchar in ustring:
		inside_code=ord(uchar)
		if inside_code == 12288:
		#全角空格直接转换
			inside_code = 32 
		elif (inside_code >= 65281 and inside_code <= 65374):
			#全角字符（除空格）根据关系转化
			inside_code -= 65248

		rstring += chr(inside_code)
	# if rstring != ustring:
	# 	print('corrected')
	return rstring

class dataset(object):
	"""用来处理文本数据，并且提供数据"""
	def __init__(self, filename, shuffle = True, max_iter = None, idlized = False):
		'''
		Args:
			filename: path to the data file
			max_iter: max number of sentences to yield from this dataset
		'''
		self.shuffle = shuffle
		self.filename = filename
		self.max_iter = max_iter
		self.length = None
		self.idlized = idlized
		self.data = []
		for(event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2) in self:
			self.data.append((event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2))


	def __iter__(self):
		'''
		支持iteration的遍历操作，每次得到一个句子的词和标注
		Return:
			words[w1, w2, ..., wn]
			tags:[t1, t2, ..., tn]
		'''
		count = 0
		with open(self.filename) as f:
			chars, entity_sub_types, entity_types, roles, distances_1, distances_2 = [], [], [], [], [], []
			event_type, argument_role = None, None
			datas = []
			for line in f:
				# print line
				line = line.strip()
				# 如果是空行，表示句子开始
				if len(line) == 0:
					count += 1
					if self.max_iter is not None and count > self.max_iter:
						break
					yield event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2
					chars, entity_sub_types, entity_types, roles, distances_1, distances_2 = [], [], [], [], [], []
					event_type, argument_role = None, None
				else:
					elems = line.split(' ')
					# 如果需要进行数字化
					if self.idlized:
						elems = [int(x) for x in elems]
					# 如果是信息行
					if len(elems) == 2:
						event_type, argument_role = elems[:2]
					# 如果是token标注行
					elif len(elems) == 6:
						chars.append(elems[0])
						entity_sub_types.append(elems[1])
						entity_types.append(elems[2])
						roles.append(elems[3])
						distances_1.append(elems[4])
						distances_2.append(elems[5])



	def __len__(self):
		'''
		查看句子数量
		'''
		if self.length is None:
			self.length = 0
			for _ in self:
				self.length += 1
		return self.length

	def get_minibatch(self, batch_size):
		'''
		从数据集中获取minibatch
		Args:
			batch_size: number of sentences in a batch
		'''
		if self.shuffle:
			random.shuffle(self.data)		
		event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch = [], [], [], [], [], [], [], []
		for(event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2) in self.data:
			if len(event_type_batch) == batch_size:
				yield event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch
				event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch = [], [], [], [], [], [], [], []

			event_type_batch.append(event_type)
			argument_role_batch.append(argument_role)
			chars_batch.append(chars)
			entity_sub_types_batch.append(entity_sub_types)
			entity_types_batch.append(entity_types)
			roles_batch.append(roles)
			distances_1_batch.append(distances_1)
			distances_2_batch.append(distances_2)


		# 处理最后不足一个batch的情况
		if len(event_type_batch) > 0:
			yield event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch


if __name__ == '__main__':
	# devset = dataset('../../data/original/layer.6.5.4.3.2.1/dev.id.lm.full.txt')
	train_dataset = dataset(train_data_id_path, idlized = True)
	# for event in train_dataset:
	# 	print(event)
	# 	break
	# print(len(train_dataset))
	# print(train_dataset)


	for event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch in train_dataset.get_minibatch(4):
		print(event_type_batch)

		# 每个句子
		# for x in x_batch:
		# 	print xs
		# print(x_batch, y_batch)
		break

	# for i,(x_batch, y_batch, embedding_batch, segs_batch, words_batch) in enumerate(devset.get_minibatch_with_lm_embedding(100)):
	# 	print(i)
	# 	print(len(embedding_batch)) # 100
	# 	# print(embedding_batch[0])
	# 	print(len(embedding_batch[0][0])) # 4608
	# 	print(len(segs_batch)) # 100
	# 	print(len(segs_batch[0][0]))
	# 	for x, segs in zip(x_batch, segs_batch):
	# 		if len(x) != len(segs):
	# 			raise Exception('长度不一致')
	# 	# print(seqs_batch[0])
		
		# # 每个句子
		# for x, words in zip(x_batch, words_batch):
		# 	print(len(x))
		# 	print(len(words))
		# 	# 每个字
		# 	for char_id, word_id in zip(x, words):
		# 		print(char_id, word_id)
		# 		# pass
		# 	break
		# break


