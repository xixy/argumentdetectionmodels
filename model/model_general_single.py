#coding=utf-8
from .base_model import *
import numpy as np
from .hyperparameter import *
import sys
sys.path.append(os.path.abspath('.'))
from ArgumentDetectionModels.configure import *

class cnn_model(base_model):
	"""
	要么不用，要么全都用
	"""
	def __init__(self, hp, logger, vocabs, args, char_embeddings = None,word_embeddings = None):
		'''
		初始化
		Args:
			hp: 超参数
			logger: 日志
			vocabs: [char_vocab, event_type_vocab, argument_role_vocab, entity_type_vocab, entity_sub_type_vocab]
			args: 状态
			char_embeddings: 字向量
			word_embeddings: 词向量
		'''
		super(cnn_model, self).__init__(hp, logger)
		self.char_embeddings_vocab = char_embeddings
		self.vocabs = vocabs
		self.char_vocab, self.event_type_vocab, self.argument_role_vocab, self.argument_role_bio_vocab, self.entity_type_vocab, \
			self.entity_sub_type_vocab = vocabs
		self.args = args

	def get_variables_for_training(self):
		'''

		返回需要优化的参数
		'''
		all_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)

		return all_vars



	def build(self):
		'''
		构建可计算图
		'''
		# 添加placeholders
		self.add_placeholders()
		# 添加向量化操作，得到每个词的向量表示
		self.add_word_embeddings_op()
		# 计算logits
		self.add_logits_op()
		# 计算概率
		self.add_pred_op()
		# 计算损失
		self.add_loss_op()

		# 定义训练操作
		self.add_train_op(self.hp.optimizer, self.lr, self.loss,
			self.hp.clip)
		# 创建session和logger
		self.initialize_session() 



	def add_placeholders(self):
		'''
		定义placeholder，所有的变量
		'''
		# 表示batch中每个句子的字的id表示
		# shape = (batch size, max length of sentence in batch)
		self.char_ids = tf.placeholder(tf.int32, shape = [None, None], name = "char_ids")

		# 表示batch中每个句子的长度
		# shape = (batch size,)
		self.sequence_lengths = tf.placeholder(tf.int32, shape = [None], name = "sequence_lengths")

		# 表示batch中每个句子的事件类型
		# shape = (batch size)
		self.event_type_labels = tf.placeholder(tf.int32, shape = [None], name = 'event_type_labels')

		# 表示batch中每个句子的论元类型
		# shape = (batch size)
		self.argument_role_labels = tf.placeholder(tf.int32, shape = [None], name = 'argument_role_labels')

		# 表示batch中每个句子的每个字的entity type
		# shape = (batch size, max length of sentence in batch)
		self.entity_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "entity_type_labels")

		# 表示batch中每个句子的每个字的entity sub type
		# shape = (batch size, max length of sentence in batch)
		self.entity_sub_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "entity_sub_type_labels")

		# 表示batch中每个句子的每个字的argument role
		# shape = (batch size, max length of sentence in batch)
		self.argument_role_labels_per_token = tf.placeholder(tf.int32, shape = [None, None], name = "argument_role_labels_per_token")

		# 表示batch中每个句子的每个字的distance 1
		# shape = (batch size, max length of sentence in batch)
		self.distance_1_labels = tf.placeholder(tf.int32, shape = [None, None], name = "distance_1_labels")

		# 表示batch中每个句子的每个字的distance 2
		# shape = (batch size, max length of sentence in batch)
		self.distance_2_labels = tf.placeholder(tf.int32, shape = [None, None], name = "distance_2_labels")

		# dropout
		self.dropout = tf.placeholder(tf.float32, shape = [], name = "dropout")
		# 学习率
		self.lr = tf.placeholder(tf.float32, shape = [], name = "lr")

		self.dimension = 0

		# 
		# shape = (batch size, max length of sentence in batch)
		# self.seg_ids = tf.placeholder(tf.int32, shape = [None, None], name = 'seg_ids')

		# self.word_ids = tf.placeholder(tf.int32, shape = [None, None], name = "word_ids")

		# # LM embedding 语言模型的embedding
		# # shape = (batch size, max length of sentence in batch, )
		# self.lm_embeddings = tf.placeholder(tf.float32, shape = [None, None, lm_embedding_dim * len(bert_layers)], name = 'lm_embedding')

	def add_word_embeddings_op(self):
		'''
		添加embedding操作，包括词向量和字向量
		如果self.embeddings不是None，那么词向量就采用pre-trained vectors，否则自行训练
		字向量是自行训练的
		'''
		
		# 1. add char embedding
		self.logger.info("加入字向量")
		with tf.variable_scope("chars"):
			# 如果词向量是None
			if self.char_embeddings_vocab is None:
				self.logger.info("WARNING: randomly initializing word vectors")
				_char_embeddings = tf.get_variable(
					name = '_char_embeddings',
					dtype = tf.float32,
					shape = [len(self.char_vocab), self.hp.char_dimension]
					)
			else:
				# 加载已有的词向量
				_char_embeddings = tf.Variable(
					self.char_embeddings_vocab,
					name = '_char_embeddings',
					dtype = tf.float32,
					trainable = False
					)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			char_embeddings = tf.nn.embedding_lookup(
				_char_embeddings,
				self.char_ids,
				name = 'char_embeddings'
				)

			self.dimension += self.hp.char_dimension

		# 2. 加入position embedding
		self.logger.info('加入距离向量1')
		with tf.variable_scope("distance_1"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_distance_1_embeddings = tf.get_variable(
				name = '_distance_1_embeddings',
				dtype = tf.float32,
				shape = [self.hp.distance_max - self.hp.distance_min, self.hp.distance_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			distance_1_embeddings = tf.nn.embedding_lookup(
				_distance_1_embeddings,
				self.distance_1_labels,
				name = 'distance_1_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, distance_1_embeddings], axis = -1)

			self.dimension += self.hp.distance_embedding_dimension

		# 3. 加入position embedding
		self.logger.info('加入距离向量2')
		with tf.variable_scope("distance_2"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_distance_2_embeddings = tf.get_variable(
				name = '_distance_2_embeddings',
				dtype = tf.float32,
				shape = [self.hp.distance_max - self.hp.distance_min, self.hp.distance_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			distance_2_embeddings = tf.nn.embedding_lookup(
				_distance_2_embeddings,
				self.distance_2_labels,
				name = 'distance_2_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, distance_2_embeddings], axis = -1)
			self.dimension += self.hp.distance_embedding_dimension

		# 4. 加入entity type embedding
		self.logger.info('加入实体类型向量')
		with tf.variable_scope("entity"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_entity_embeddings = tf.get_variable(
				name = '_entity_embeddings',
				dtype = tf.float32,
				shape = [len(self.entity_sub_type_vocab), self.hp.entity_subtype_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			entity_embeddings = tf.nn.embedding_lookup(
				_entity_embeddings,
				self.entity_sub_type_labels,
				name = 'entity_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, entity_embeddings], axis = -1)
			self.dimension += self.hp.entity_subtype_embedding_dimension

		# 5. 加入argument role 
		if self.hp.add_argument_role == 1:
			self.logger.info('加入论元类型向量')
			with tf.variable_scope("arguments"):
				self.logger.info("WARNING: randomly initializing word vectors")
				_argument_role_embeddings = tf.get_variable(
					name = '_argument_role_embeddings',
					dtype = tf.float32,
					shape = [len(self.argument_role_bio_vocab), self.hp.argument_role_embedding_dimension],
					trainable = True
					)
				# lookup来获取word_ids对应的embeddings
				# shape = (batch size, max_length_sentence, dim)
				argument_role_embeddings = tf.nn.embedding_lookup(
					_argument_role_embeddings,
					self.argument_role_labels_per_token,
					name = 'argument_role_embeddings'
					)
				char_embeddings = tf.concat([char_embeddings, argument_role_embeddings], axis = -1)
				self.dimension += self.hp.argument_role_embedding_dimension

		# # shape = (batch size, max_length_sentence, dim)
		self.char_embeddings = tf.nn.dropout(char_embeddings, self.dropout)

		# self.char_embedding_dimension = self.hp.char_dimension + 2 * self.hp.distance_embedding_dimension \
		# 	+ self.hp.entity_subtype_embedding_dimension + self.hp.argument_role_embedding_dimension


	def add_logits_op(self):
		'''
		定义self.logits，句子中的每个词都对应一个得分向量，维度是tags的维度
		'''
		# 首先对句子进行卷积
		with tf.variable_scope('cnn'):
			# shape (batch size, max sentence length, kernel nums)
			feature_maps = tf.layers.conv1d(
				inputs = self.char_embeddings,
				kernel_size = self.hp.cnn_kernel_size,
				filters = self.hp.cnn_kernel_nums,
				padding = 'same',
				use_bias = True,
				activation = tf.nn.tanh,
				name = 'convolution'
				)
		# 进行pooling
		with tf.variable_scope('poolinig'):
			# shape = (batch size, 1, kernel nums)
			pooled_output = tf.layers.max_pooling1d(
				inputs = feature_maps,
				pool_size = self.hp.max_length_sentence,
				strides = 1,
				padding = 'valid',
				name = 'max_pooling'
				)

		# shape = (batch size, kernel nums)
		output = tf.reshape(pooled_output, [-1, self.hp.cnn_kernel_nums])
		
		# 添加事件type embedding
		self.logger.info('加入事件类型向量')
		with tf.variable_scope('event_type'):
			self.logger.info("WARNING: randomly initializing word vectors")
			_event_type_embeddings = tf.get_variable(
				name = '_event_type_embeddings',
				dtype = tf.float32,
				shape = [len(self.event_type_vocab), self.hp.event_type_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, dim)
			event_type_embeddings = tf.nn.embedding_lookup(
				_event_type_embeddings,
				self.event_type_labels,
				name = 'event_type_embeddings'
				)
			self.dimension += self.hp.event_type_embedding_dimension
		# shape = (batch size, dimension + event_type_embedding_dimension)
		output = tf.concat([event_type_embeddings, output], axis = -1)

		# 然后用全联接网络计算概率
		with tf.variable_scope('proj'):
			W = tf.get_variable(
				name = 'w', 
				dtype = tf.float32,
				shape = [self.hp.cnn_kernel_nums + self.hp.event_type_embedding_dimension, len(self.argument_role_vocab)]
				)
			b = tf.get_variable(
				name = 'b',
				dtype = tf.float32,
				shape = [len(self.argument_role_vocab)],
				initializer = tf.zeros_initializer()
				)
			# shape = (batch size, vocab_tags_nums)
			self.logits = tf.matmul(output, W) + b

	def add_pred_op(self):
		'''
		计算prediction，如果使用crf的话，需要
		'''
		# 取出概率最大的维度的idx
		
		# if not self.hp.use_crf:
		# shape = (batch size,)
		self.labels_pred = tf.cast(tf.argmax(self.logits, axis=-1), tf.int32)

	def add_loss_op(self):
		'''
		计算损失
		'''
		# shape = (batch size)
		losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
			logits = self.logits,
			labels = self.argument_role_labels
			)
		# mask = tf.sequence_mask(self.sequence_lengths)
		# losses = tf.boolean_mask(losses, mask)
		self.loss = tf.reduce_mean(losses)

		# for tensorboard
		tf.summary.scalar('loss', self.loss)


	def run_epoch(self, train, dev, epoch):
		'''
		运行一个epoch，包括在训练集上训练、dev集上测试，一个epoch中对train集合的所有数据进行训练
		'''
		batch_size = self.hp.batch_size
		nbatches = (len(train) + batch_size - 1) // batch_size

		# chars_batch, labels_batch, labels_17_batch, labels_3_batch, lm_embeddings_batch, segs_batch, words_batch
		for i, (event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch) in enumerate(train.get_minibatch(batch_size)):

			# 检查wo
			# 构造feed_dict,主要是包括：
			# 1. word_ids, word_length
			# 2. char_ids, char_length
			# 3. learning rate
			# 4. dropout keep prob
			fd, _ = self.get_feed_dict(event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch,self.hp.learning_rate, self.hp.dropout)

			# 执行计算

			# train_loss = self.sess.run(
			# 	[self.loss],
			# 	feed_dict = fd
			# 	)	
			_, train_loss, summary = self.sess.run(
				[self.train_op, self.loss, self.merged],
				feed_dict = fd
				)

			# tensorboard
			if i % 10 == 0:
				self.file_writer.add_summary(summary, epoch * nbatches + i)

		# 在dev集上面进行测试
		metrics = self.run_evaluate(dev)

		msg = " - ".join(["{} {:04.2f}".format(k, v)
			for k, v in metrics.items()])
		self.logger.info(msg)

		# 返回f1
		return metrics["r2"]

	def pad_sequences(self, sequences, pad_token, nlevels = 1):
		'''
		对sequence进行填充
		Args:
			sequences: a generator of list or tuple
			pad_token: the token to pad with
			nlevels: padding的深度，如果是1，则表示对词进行填充，如果是2表示对字进行填充
		Return:
			a list of list where each sublist has same length
		'''
		# print '--------pad-sequences--------'
		if nlevels == 1:
			# 找到sequences中的句子最大长度
			# max_length_sentence = max(map(lambda x: len(x), sequences))
			# 然后直接进行padding
			sequences_padded, sequences_length = self._pad_sequences(sequences, 
				pad_token, self.hp.max_length_sentence)

		if nlevels == 2:
			# 找到sequence中所有句子的所有单词中字母数最大的单词
			max_length_word = max([max(map(lambda x: len(x), seq)) for seq in sequences])
			# print max_length_word
			sequences_padded, sequences_length = [], []
			for seq in sequences:
				# print seq
				# 将每个句子的每个词都进行填充
				sp, sl = self._pad_sequences(seq, pad_token, max_length_word)
				# print sp, sl
				# 每个句子的字母的表示
				sequences_padded += [sp]
				# 每个句子的字母的长度
				sequences_length += [sl]
			# 然后对句子进行填充
			# batch中最大长度的句子
			max_length_sentence = max(map(lambda x : len(x), sequences))
			# 填充的时候用[0,0,0,0,0]用字母向量进行填充
			sequences_padded, _ = self._pad_sequences(sequences_padded, 
				[pad_token] * max_length_word, max_length_sentence)
			# 得到句子的每个单词的字母的长度 (batch, max_length_sentence, letter_length)
			sequences_length, _ = self._pad_sequences(sequences_length, 0, max_length_sentence)

		return sequences_padded, sequences_length



	def _pad_sequences(self, sequences, pad_token, max_length):
		'''
		对sequences进行填充
		Args:
			pad_token: the token to pad with
		'''
		sequences_padded, sequences_lengths = [], []
		for sequence in sequences:
			sequence = list(sequence)
			# 获取句子长度
			sequences_lengths += [min(len(sequence), max_length)]
			# 进行填充
			sequence = sequence[:max_length] + [pad_token] * max(max_length - len(sequence), 0)
			sequences_padded += [sequence]
		return sequences_padded, sequences_lengths

	def pad_lm_embedding(self, lm_embeddings, pad_token):
		'''
		对语言模型进行padding
		'''
		# 找到batch中句子最大长度
		max_length_sentence = max(map(lambda x: len(x), lm_embeddings))	
		# print('最大句子长度为'+ str(max_length_sentence))
		# 然后直接进行padding
		sequences_padded, sequences_length = self._pad_sequences(lm_embeddings, 
			[pad_token] * lm_embedding_dim * len(bert_layers), max_length_sentence)
		return sequences_padded, sequences_length


	def get_feed_dict(self, event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, lr = None, dropout = None):
		'''
		Args:
			lr: learning rate
			dropout: keep prob
		'''
			
		chars_batch_, sequence_lengths = self.pad_sequences(chars_batch, 0)
		entity_sub_types_batch_, _ = self.pad_sequences(entity_sub_types_batch, self.entity_sub_type_vocab['O'])
		entity_types_batch_, _ = self.pad_sequences(entity_types_batch, self.entity_type_vocab['O'])
		roles_batch_, _ = self.pad_sequences(roles_batch, self.argument_role_vocab['O'])
		distances_1_batch_ = [[x - self.hp.distance_min for x in sentence_distance] for sentence_distance in distances_1_batch]
		distances_2_batch_ = [[x - self.hp.distance_min for x in sentence_distance] for sentence_distance in distances_2_batch]		
		distances_1_batch_, _ = self.pad_sequences(distances_1_batch_, self.hp.distance_max - self.hp.distance_min - 1)
		distances_2_batch_, _ = self.pad_sequences(distances_2_batch_, self.hp.distance_max - self.hp.distance_min - 1)

		feed = {
			self.char_ids: chars_batch_, # 句子的词的id表示(batch, max_length_sentence)
			self.sequence_lengths: sequence_lengths, # 句子的词的长度(batch, )
			self.event_type_labels: event_type_batch,
			self.argument_role_labels : argument_role_batch,
			self.entity_type_labels : entity_types_batch_,
			self.entity_sub_type_labels: entity_sub_types_batch_,
			self.argument_role_labels_per_token:roles_batch_,
			self.distance_1_labels: distances_1_batch_,
			self.distance_2_labels: distances_2_batch_,
		}

		if lr is not None:
			feed[self.lr] = lr
		if dropout is not None:
			feed[self.dropout] = dropout
		# feed = {}
		
		return feed, sequence_lengths


		


	def run_evaluate(self, test, output_all_file = None, output_wrong_file = None, output_class_wrong_file = None):
		'''
		在测试集上运行，并且统计结果，包括precision/recall/accuracy/f1
		Args:
			test: 一个dataset instance
		Returns:
			metrics: dict metrics['acc'] = 98.4
		'''
		accs = []
		# 进行identification的记录
		correct_preds_id, total_correct_id, total_preds_id = 0., 0., 0. 
		# 进行classification的记录
		correct_preds_cl, total_correct_cl, total_preds_cl = 0., 0., 0.
		
		# idx_to_tag = {idx : tag for tag, idx in self.vocab_tags.items()}
		# idx_to_token = {idx: token for token, idx in self.vocab_chars.items()}
		for (event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch) in test.get_minibatch(self.hp.batch_size):
			# predict_batch
			# shape = (batch size, max_length_sentence)
			# shape = (batch size,)
			idx = -1 # 表示这个batch中句子的index
			labels_pred = self.predict_batch(event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch)
			for lab, lab_pred in zip(argument_role_batch, labels_pred):
				# 计算identification
				total_correct_id += 1
				total_correct_cl += 1

				if lab == lab_pred:
					correct_preds_cl += 1
				if lab_pred != self.argument_role_vocab['O']:
					correct_preds_id += 1

		# 计算recall
		r1 = correct_preds_id / total_correct_id if correct_preds_id > 0 else 0
		r2 = correct_preds_cl / total_correct_cl if correct_preds_cl > 0 else 0
		# 返回结果
		return {
			'r1': 100 * r1,
			'r2': 100 * r2,
		}

			# for lab, lab_pred, length, logits in zip(labels, labels_pred, sequence_lengths, logits_sequences):
			# 	idx += 1
			# 	# 取每一句话，长度为length
			# 	# 正确label
			# 	lab = lab[:length]
			# 	# 预测得到的label
			# 	lab_pred = lab_pred[:length]


			# 	# 预测正确的个数
			# 	accs += [a == b for (a, b) in zip(lab, lab_pred)]

			# 	# 真实chunk
			# 	lab_chunks = set(self.get_chunks(lab, self.vocab_tags))
			# 	# 预测chunk
			# 	lab_pred_chunks = set(self.get_chunks(lab_pred, self.vocab_tags))

			# 	# 1. 计算identification效果

			# 	# 真实结果
			# 	chunks_without_label = set([(item[1], item[2]) for item in lab_chunks])
			# 	# 预测结果
			# 	chunks_pred_without_label = set([(item[1], item[2]) for item in lab_pred_chunks])
			# 	# 记录正确的chunk数量
			# 	correct_preds_id += len(chunks_without_label & chunks_pred_without_label)
			# 	# 预测出的chunk数量
			# 	total_preds_id += len(chunks_pred_without_label)
			# 	# 正确的chunk数量
			# 	total_correct_id += len(chunks_without_label)

			# 	# 2. 计算classification效果

			# 	# 记录正确的chunk数量
			# 	correct_preds_cl += len(lab_chunks & lab_pred_chunks)
			# 	# 预测出的chunk数量
			# 	total_preds_cl += len(lab_pred_chunks)
			# 	# 正确的chunk数量
			# 	total_correct_cl += len(lab_chunks)

			# 	# 3. 进行输出
			# 	# 3.1 输出所有结果
			# 	line = words[idx]
			# 	if output_all_file != None:
			# 		for char,tag, tag_pred, logit in zip(line,lab, lab_pred, logits):
			# 			token = idx_to_token[char]
			# 			tag = idx_to_tag[tag]
			# 			tag_pred = idx_to_tag[tag_pred]
			# 			output_all_file.write(token + '	' + tag + '	' + tag_pred + '\n')
			# 			output_all_file.write(','.join([str(x) for x in logit])+'\n')
			# 		output_all_file.write('\n')
			# 	# 3.2 输出错误结果
			# 	if output_wrong_file != None and lab != lab_pred:
			# 		for char,tag, tag_pred in zip(line,lab, lab_pred):
			# 			token = idx_to_token[char]
			# 			tag = idx_to_tag[tag]
			# 			tag_pred = idx_to_tag[tag_pred]
			# 			output_wrong_file.write(token + '	' + tag + '	' + tag_pred + '\n')
			# 		output_wrong_file.write('\n')

			# 	# 3.3 输出识别正确但是分类错误的结果
			# 	if output_class_wrong_file != None and len(chunks_without_label & chunks_pred_without_label) != len(lab_chunks & lab_pred_chunks):
			# 		for char,tag, tag_pred in zip(line,lab, lab_pred):
			# 			token = idx_to_token[char]
			# 			tag = idx_to_tag[tag]
			# 			tag_pred = idx_to_tag[tag_pred]
			# 			output_class_wrong_file.write(token + '	' + tag + '	' + tag_pred + '\n')
			# 		output_class_wrong_file.write('\n')

				# # 4 输出logits
				# if output_all_file != None:
				# 	output_all_file.write(logits+'\n')

		# # 1. 计算trigger identification的结果
		# # 计算precision，预测出的chunk中有多少是正确的
		# p1 = correct_preds_id / total_preds_id if correct_preds_id > 0 else 0
		# # 计算recall，预测正确的chunk占了所有chunk的数量
		# r1 = correct_preds_id / total_correct_id if correct_preds_id > 0 else 0
		# # 计算f1
		# f1_1 = 2 * p1 * r1 / (p1 + r1) if correct_preds_id > 0 else 0

		# # 2. 计算trigger classification的结果
		# # 计算precision，预测出的chunk中有多少是正确的
		# p2 = correct_preds_cl / total_preds_cl if correct_preds_cl > 0 else 0
		# # 计算recall，预测正确的chunk占了所有chunk的数量
		# r2 = correct_preds_cl / total_correct_cl if correct_preds_cl > 0 else 0
		# # 计算f1
		# f1_2 = 2 * p2 * r2 / (p2 + r2) if correct_preds_cl > 0 else 0
		# # 计算accuracy，用预测对的词的比例来进行表示
		# acc = np.mean(accs)

		# # 返回结果
		# return {
		# 	'p1': 100 * p1,
		# 	'r1': 100 * r1,
		# 	'f1_1': 100 * f1_1,
		# 	'p2': 100 * p2,
		# 	'r2': 100 * r2,
		# 	'f1_2': 100 * f1_2
		# }



	def predict_batch(self, event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch):
		'''
		对一个batch进行预测，并返回预测结果
		Args:
			words: list of sentences
		Returns:
			labels_pred: list of labels for each sentence
		'''
		logits_sequences = []
		# chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch
		fd, sequence_lengths = self.get_feed_dict(event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, dropout = 1.0)

		# if self.hp.use_crf:
		# 	viterbi_sequences = []
		# 	logits, trans_params = self.sess.run(
		# 		[self.logits, self.trans_params],
		# 		feed_dict = fd
		# 		)
		# 	for logit, sequence_length in zip(logits, sequence_lengths):
		# 		logit = logit[:sequence_length]
		# 		# print(logit)
		# 		# print(len(logit))
		# 		# print(logit[0])
		# 		viterbi_seq, viterbi_score = tf.contrib.crf.viterbi_decode(
		# 			logit, trans_params)
		# 		viterbi_sequences += [viterbi_seq]
		# 		logits_sequences.append(logit)

		# 	return viterbi_sequences, sequence_lengths, logits_sequences

		# else:
		# shape = (batch size, max_length_sentence)
		labels_pred = self.sess.run(self.labels_pred, feed_dict = fd)

		return labels_pred

if __name__ == '__main__':
	model = seq_tag_model(None, None, [None,None,None], None)
	seq = [4, 4, 5, 0, 3, 5]
	tags = {
			'B-PER' : 4,
			'I-PER' : 5,
			'B-LOC' : 3,
			'O' : 0
			}
	print(model.get_chunks(seq, tags))

