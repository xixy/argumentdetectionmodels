# $1 gpu
# $2 是否添加argument role

lambda_1_vars=(1 2 3 4)
lambda_2_vars=(1)

for lambda_1 in ${lambda_1_vars[@]}
do
	for lambda_2 in ${lambda_2_vars[@]}
	do
		echo $lambda_1
		echo $lambda_2
		python train_single.py --dirpath /data/xxy/arguments/single/results/$2/$lambda_1/$lambda_2 --add_nvidia $1 --add_argument_role $2
	done
done