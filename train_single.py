# coding=utf-8
import sys
sys.path.append('./model/bert_as_feature/bert')
from configure import *
from model.vocab import *
from model.model_general_single import *
from model.hyperparameter import *
from model.log_util import *
import argparse
from model.dataset import *

def main(args):
	'''
	构造模型
	构造数据
	进行训练
	保存模型
	'''
	# 1. 加载vocab和embeddings

	char_vocab = load_vocab(char_vocab_path)
	event_type_vocab = load_vocab(event_type_vocab_path)
	argument_role_vocab = load_vocab(argument_role_vocab_path)
	argument_role_bio_vocab = load_vocab(argument_role_vocab_bio_path)
	entity_type_vocab = load_vocab(entity_type_vocab_path)
	entity_sub_type_vocab = load_vocab(entity_sub_type_vocab_path)
	char_embeddings = get_trimmed_word2vec(trimmed_char_word2vec_path)

	# 2. 得到id化的训练数据
	train_dataset = dataset(train_data_id_path, idlized = True)
	test_dataset = dataset(test_data_id_path, idlized = True)
	dev_dataset = dataset(dev_data_id_path, idlized = True)

	# 3. 构造模型
	hp = hyperparameter(args)
	logger = get_logger(hp.log_path)
	vocabs = [char_vocab, event_type_vocab, argument_role_vocab, argument_role_bio_vocab, entity_type_vocab, entity_sub_type_vocab]
	
	model = cnn_model(hp, logger, vocabs, args, char_embeddings)

	# # 4 训练
	logger.info('开始训练')
	model.build()
	model.train(train_dataset, test_dataset)
	logger.info('训练结束')

	# 5. 测试
	logger.info('开始测试')
	model.restore_session()

	output_all_file = open(model.hp.evaluate_output_all_file, 'w')
	output_wrong_file = open(model.hp.evaluate_output_wrong_file, 'w')
	output_class_wrong_file = open(model.hp.evaluate_output_class_wrong_file, 'w')

	model.evaluate(test_dataset, output_all_file, output_wrong_file, output_class_wrong_file)
	output_all_file.close()
	output_wrong_file.close()
	output_class_wrong_file.close()

	logger.info('测试结束')


if __name__ == '__main__':
	# python train_single.py --dirpath '../results/result.1' --add_nvidia -1 --add_argument_role 0
	parser = argparse.ArgumentParser()
	parser.add_argument('--dirpath', required=True, type = str, help='输出路径')
	parser.add_argument('--add_nvidia',required=True, type = str, help='选择显卡编号')
	parser.add_argument('--add_argument_role', required=True, type = int, help='是否添加argument role')
	# 解析参数
	args = parser.parse_args()
	print(args.dirpath)
	# 设置显卡，如果是-1，就不使用显卡,使用CPU进行运算
	if args.add_nvidia in ['0', '1', '2', '3']:
		os.environ['CUDA_VISIBLE_DEVICES']=args.add_nvidia


	# args = parser.parse_args()
	main(args)






